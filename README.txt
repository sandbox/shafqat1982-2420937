=====
Contextual body Class
http://drupal.org/project/cbodyclass
-----

-- About --

This module add contextual classes and responsive classes to HTML body which can be very helpful to style differently on different context.
It also add responsive classes on different window widths like:
cbody-layout-mobile, cbody-layout-narrow, cbody-layout-normal, cbody-layout-wide



=====
Installation
-----

1. Enable the module.
2. You will get all dynamic classes via JS in HTML body.
2. If want these dynamic classes even in disable JS browser, Insert a PHP snippet to your theme's page.tpl.php file(s) that prints the $body_classes variable.