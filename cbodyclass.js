(function($) {
    
    
    Drupal.behaviors.cbodyclass = function() {

    // Getting classes from hidden element to body tag

    if ($("#hiddenbodyclassesdata").length) {
        var bodyclasses = $("#hiddenbodyclassesdata").text();
        $('body').addClass(bodyclasses);
        $("#hiddenbodyclassesdata").remove();
        $("#hiddenbodyclassesdata1").remove();
    }
    else if ($("#hiddenbodyclassesdata1").length) {
        var bodyclasses = $("#hiddenbodyclassesdata1").text();
        $('body').addClass(bodyclasses);
        $("#hiddenbodyclassesdata").remove();
        $("#hiddenbodyclassesdata1").remove();
    }

    // adding responsive class on window load
    addingresponsiveclasses();
    
    // updating responsive class on window resize
    var resizeTimer;
    $(window).resize(function() {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(addingresponsiveclasses, 250);
    });
    
   // addiing responsive classes 
    function addingresponsiveclasses() {
        
        var wwidth = $(window).width();
        
        // responsive classes 
        var layoutclss = "cbody-layout-mobile cbody-layout-narrow cbody-layout-normal cbody-layout-wide";
        
        //check window width replacing relevant class
        if (wwidth < 740 && !$("body.cbody-layout-mobile").length) {
            
            $('body').removeClass(layoutclss).addClass("cbody-layout-mobile");
            
        } else if (wwidth >= 740 && wwidth < 980 && !$("body.cbody-layout-narrow").length) {

            $('body').removeClass(layoutclss).addClass("cbody-layout-narrow");
            
        } else if (wwidth >= 980 && wwidth < 1220 && !$("body.cbody-layout-normal").length) {
   
            $('body').removeClass(layoutclss).addClass("cbody-layout-normal");

        } else if (wwidth >= 1220 && !$("body.cbody-layout-wide").length) {

            $('body').removeClass(layoutclss).addClass("cbody-layout-wide");
        }
    }

};
})(jQuery);
